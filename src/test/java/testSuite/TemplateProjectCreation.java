package testSuite;

import java.io.File;
import java.util.Iterator;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.testng.annotations.Test;

import utility.CoreUtil;
import utility.LoggerUtil;
import utility.SeleniumTools;

public class TemplateProjectCreation {

	static LoggerUtil loggerUtil = new LoggerUtil(TemplateProjectCreation.class);

	@Test(description = "Create a project using rollforward option(Use the above project)  with content and observe the data in WP and Permanent files data is visible (same as previous project)", enabled = true)
	public void AUL_TC_15() {
		try {
			Workbook workbook = null;
			// String str = null;
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://devrearch.auditlobby.com");
			SeleniumTools.ClearAndSetText("xpath", ".//*[@id='username']", "automation@yopmail.com");
			SeleniumTools.ClearAndSetText("xpath", ".//*[@id='password']", "Abhra@123");
			SeleniumTools.scrollToElementAndClick("xpath", "//*[contains(text(),'Login')]");
			SeleniumTools.clickOnObject("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			CoreUtil.imSleepy(2000);
			workbook = WorkbookFactory.create(
					new File("E:\\soft\\Subbaraju-workspace\\SOXLOBBY\\src\\test\\resources\\Excel\\AuditLobby.xls"));
			Sheet sheet = workbook.getSheetAt(0);
			// int rowcount = sheet.getLastRowNum();
			DataFormatter dataFormatter = new DataFormatter();
			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// Now let's iterate over the columns of the current row
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					String cellValue = dataFormatter.formatCellValue(cell);
					SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create Project']");
					// Selecting Client Name
					SeleniumTools.selectByTextByIndex("xpath",
							"//div[@class='container-fluid']//select[@name='clientName']", 1);
					// Selecting Engagement type
					SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='tOE']",
							1);
					// Selecting Engagement partner role
					SeleniumTools.selectByTextByIndex("xpath",
							"//div[@class='container-fluid']//select[@name='EngPartnerRole']", 1);
					// Data filling in project name
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='nameAsLink']",
							cellValue);
					// Data filling ProjectID
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='clientId']",
							SeleniumTools.getRandomString());
					// Data filling Fee
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='fee']",
							SeleniumTools.getRandomNumbers());
					// Data filling Address
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='address']",
							SeleniumTools.getRandomString());
					// selecting date Dec 31
					SeleniumTools.clickOnObject("xpath", "//div[@class='container-fluid']//input[@name='fiscalYear']");
					Thread.sleep(1000);
					SeleniumTools.clickOnObject("xpath",
							"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[3]/td[4]");
					SeleniumTools.clickOnObject("xpath",
							"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[6]/td[2]");

					SeleniumTools.clickOnObject("xpath", "//span[text()='Next step']");
					Thread.sleep(1000);
					SeleniumTools.clickOnObject("xpath",
							"//div[@class='col-md-4 col-sm-4 roll-align']//input[@name='choose_template']");
					Thread.sleep(1000);
					SeleniumTools.clickOnObject("xpath", "//input[@type='checkbox']");
					CoreUtil.imSleepy(1000);
					SeleniumTools.clickOnObject("xpath", "//span[text()='Create']");
					SeleniumTools.getText("xpath",
							"//div[@class='ng-toast ng-toast--right ng-toast--top ']//ul/li[1]/div[@class='alert alert-success alert-dismissible']/span/span");
					SeleniumTools.getScreenshot("AUL_TC_15");
					CoreUtil.imSleepy(2000);
				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Create a blank project and Edit the Project Information")
	public void AUL_TC_11() {
		try {
			Workbook workbook = null;
			// String str = null;
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com/auditlobby");
			SeleniumTools.ClearAndSetText("xpath", ".//*[@id='username']", "automation@yopmail.com");
			SeleniumTools.ClearAndSetText("xpath", ".//*[@id='password']", "Abhra@123");
			SeleniumTools.clickOnObject("xpath", "//*[contains(text(),'Login')]");
			SeleniumTools.scrollToElementAndClick("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			workbook = WorkbookFactory.create(
					new File("E:\\soft\\Subbaraju-workspace\\SOXLOBBY\\src\\test\\resources\\Excel\\AuditLobby.xls"));
			Sheet sheet = workbook.getSheetAt(0);
			// int rowcount = sheet.getLastRowNum();
			DataFormatter dataFormatter = new DataFormatter();
			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// Now let's iterate over the columns of the current row
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					String cellValue = dataFormatter.formatCellValue(cell);
					SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create Project']");

					// Selecting Client Name
					SeleniumTools.selectByTextByIndex("xpath",
							"//div[@class='container-fluid']//select[@name='clientName']", 1);
					// Selecting Engagement type
					SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='tOE']",
							1);
					// Selecting Engagement partner role
					SeleniumTools.selectByTextByIndex("xpath",
							"//div[@class='container-fluid']//select[@name='EngPartnerRole']", 1);
					// Data filling in project name
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='nameAsLink']",
							cellValue);
					// Data filling ProjectID
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='clientId']",
							SeleniumTools.getRandomString());
					// Data filling Fee
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='fee']",
							SeleniumTools.getRandomNumbers());
					// Data filling Address
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='address']",
							SeleniumTools.getRandomString());
					// selecting date Dec 31
					SeleniumTools.clickOnObject("xpath", "//div[@class='container-fluid']//input[@name='fiscalYear']");
					Thread.sleep(1000);
					SeleniumTools.clickOnObject("xpath",
							"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[3]/td[4]");
					SeleniumTools.clickOnObject("xpath",
							"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[6]/td[2]");

					SeleniumTools.clickOnObject("xpath", "//span[text()='Next step']");

					SeleniumTools.clickOnObject("xpath", "//span[text()='Create']");
					CoreUtil.imSleepy(2000);

				}
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(enabled = false)
	public void AUL_TC_14() {
		try {
			Workbook workbook = null;
			// String str = null;
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com/auditlobby");
			SeleniumTools.ClearAndSetText("xpath", ".//*[@id='username']", "automation@yopmail.com");
			SeleniumTools.ClearAndSetText("xpath", ".//*[@id='password']", "Abhra@123");
			SeleniumTools.clickOnObject("xpath", "//*[contains(text(),'Login')]");
			SeleniumTools.clickOnObject("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			workbook = WorkbookFactory.create(
					new File("E:\\soft\\Subbaraju-workspace\\SOXLOBBY\\src\\test\\resources\\Excel\\AuditLobby.xls"));
			Sheet sheet = workbook.getSheetAt(0);
			// int rowcount = sheet.getLastRowNum();
			DataFormatter dataFormatter = new DataFormatter();
			Iterator<Row> rowIterator = sheet.rowIterator();
			while (rowIterator.hasNext()) {
				Row row = rowIterator.next();
				// Now let's iterate over the columns of the current row
				Iterator<Cell> cellIterator = row.cellIterator();
				while (cellIterator.hasNext()) {
					Cell cell = cellIterator.next();
					String cellValue = dataFormatter.formatCellValue(cell);
					// System.out.println(cellValue);

					SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create Project']");

					// Selecting Client Name
					SeleniumTools.selectByTextByIndex("xpath",
							"//div[@class='container-fluid']//select[@name='clientName']", 1);
					// Selecting Engagement type
					SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='tOE']",
							1);
					// Selecting Engagement partner role
					SeleniumTools.selectByTextByIndex("xpath",
							"//div[@class='container-fluid']//select[@name='EngPartnerRole']", 1);
					CoreUtil.imSleepy(2000);
					// Data filling in project name
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='nameAsLink']",
							cellValue);
					CoreUtil.imSleepy(2000);
					// Data filling ProjectID
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='clientId']",
							SeleniumTools.getRandomString());
					// Data filling Fee
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='fee']",
							SeleniumTools.getRandomNumbers());
					// Data filling Address
					SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='address']",
							SeleniumTools.getRandomString());
					// selecting date Dec 31
					SeleniumTools.clickOnObject("xpath", "//div[@class='container-fluid']//input[@name='fiscalYear']");
					Thread.sleep(1000);
					SeleniumTools.clickOnObject("xpath",
							"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[3]/td[4]");
					SeleniumTools.clickOnObject("xpath",
							"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[6]/td[2]");

					SeleniumTools.clickOnObject("xpath", "//span[text()='Next step']");
					Thread.sleep(1000);
					SeleniumTools.clickOnObject("xpath",
							"//div[@class='col-md-4  col-sm-4']//div[@id='templatechoose']/div[1]");
					Thread.sleep(1000);

					SeleniumTools.clickOnObject("xpath",
							"//div[@class='col-md-4  col-sm-4']//div[@id='templatechoose']/div[1]//ul/li[1]//input");
					SeleniumTools.clickOnObject("xpath", "//span[text()='Create']");
					SeleniumTools.getText("xpath",
							"//div[@class='ng-toast ng-toast--right ng-toast--top ']//ul/li[1]/div[@class='alert alert-success alert-dismissible']/span/span");
					SeleniumTools.getScreenshot("AUL_TC_14");
					CoreUtil.imSleepy(2000);

				}
			}

			SeleniumTools.closeBrowser();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
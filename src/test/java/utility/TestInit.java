package utility;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Method;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.ITestContext;
import org.testng.ITestResult;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.markuputils.ExtentColor;
import com.aventstack.extentreports.markuputils.Markup;
import com.aventstack.extentreports.markuputils.MarkupHelper;

public class TestInit {
	public static ExtentReports extent;
	public static ExtentTest pNode;
	public static WebDriver driver;
	public WebDriverWait wait;
	public static ThreadLocal<ExtentTest> parentTest = new ThreadLocal<ExtentTest>();

	@BeforeSuite(alwaysRun = true)
	public void beforeSuite(ITestContext suite) throws Exception {
		/*
		 * extent = ExtentManager.getInstance(getClass().getSimpleName()); pNode =
		 * extent.createTest(getClass().getSimpleName()); parentTest.set(pNode);
		 */

	}

	@BeforeClass(alwaysRun = true)
	public void beforeClassRun() {
		extent = ExtentManager.getInstance(getClass().getSimpleName());
		pNode = extent.createTest(getClass().getSimpleName());
		parentTest.set(pNode);
		Markup m = MarkupHelper.createLabel("Setup for Test: " + getClass().getSimpleName(), ExtentColor.BLUE);
		pNode.info(m);

	}

	@BeforeTest(alwaysRun = true)
	public void beforeTest() {

	}

	@BeforeMethod(alwaysRun = true)
	public void beforeMethod(Method method) {
		// System.out.print("START TEST: " + method.getName() + "\n");
		Markup m = MarkupHelper.createLabel("Method Name:" + method.getName(), ExtentColor.AMBER);
		pNode.info(m);
		// pNode.info(method.getName());
	}

	@AfterMethod(alwaysRun = true)
	public void afterMethod(ITestResult result) throws Exception {
		// assertions.finalizeSoftAssert();
		if (result.getStatus() == ITestResult.SUCCESS) {
			// System.out.print(result.getName() + ": Executed Successfully");
			/*
			 * MediaEntityModelProvider mediaModel =
			 * MediaEntityBuilder.createScreenCaptureFromPath(captureScreen()).build();
			 * pNode.pass("Test case is passed", mediaModel);
			 */
			// pNode.addScreenCaptureFromPath(captureScreen());
			pNode.pass(result.getName() + ":Test case is passed");
		} else if (result.getStatus() == ITestResult.FAILURE) {
			try {
				String temp=getScreenshot(driver);
				pNode.fail(result.getThrowable().getMessage(), MediaEntityBuilder.createScreenCaptureFromPath(temp).build());
				System.out.print(result.getName() + ": Execution Failed");
				String code = "Method: " + result.getName() + "\n" + "Reason: " + result.getThrowable().toString();
				Markup m = MarkupHelper.createCodeBlock(code);
				pNode.fail(m);
			} catch (Exception e) {
				e.printStackTrace();
			}
			
		} else if (result.getStatus() == ITestResult.SKIP) {
			System.out.print(result.getName() + ": Execution Skipped");
			pNode.skip(result.getName() + ":Test Case Executed Skipped!");
		}

		extent.flush();
		System.out.print("\nEND TEST: " + result.getName());
		// pNode.info(result.getTestName());
	}

	@AfterTest(alwaysRun = true)
	public void afterTest() {
		// todo write your code here
	}

	@AfterClass(alwaysRun = true)
	public void afterClassRun() {
		// Quit the driver
		// driver.close();
		driver = null;
	}

	@AfterSuite(alwaysRun = true)
	public void afterSuite() {
		// todo write your code here
		
	}

	protected void markSetupAsFailure(Exception e) {
		pNode.fail("Exception: " + e.toString());
		Assert.fail(e.getMessage());
	}

	public void markTestAsFailure(Exception e, ExtentTest pNode) {
		e.printStackTrace();
		pNode.fail("Test Case Failed due to Exception");
		pNode.error(e);
		Assert.fail(e.getMessage());
	}

	public static String captureScreen() throws Exception {
		String fileName = System.currentTimeMillis() + ".png";
		/*
		 * TakesScreenshot ts = (TakesScreenshot) driver; File source =
		 * ts.getScreenshotAs(OutputType.FILE); String destination = System
		 * .getProperty(("user.dir") + "/FailedTestsScreenshots" + screenshotName +
		 * fileName + ".png"); File fileDestination = new File(destination);
		 * FileUtils.copyFile(source, fileDestination); return destination;
		 */
		try {
			File ImgFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			FileUtils.copyFile(ImgFile, new File("./Reports/ScreenShots/" + fileName));
		} catch (Exception e) {
			System.err.println(e);
		}
		
		return "./ScreenShots/" + fileName;
	}
	
	public static String getScreenshot(WebDriver driver)
	{
		TakesScreenshot ts=(TakesScreenshot) driver;
		
		File src=ts.getScreenshotAs(OutputType.FILE);
		
		String path=System.getProperty("user.dir")+"/Screenshot/"+System.currentTimeMillis()+".png";
		
		File destination=new File(path);
		
		try 
		{
			FileUtils.copyFile(src, destination);
		} catch (IOException e) 
		{
			System.out.println("Capture Failed "+e.getMessage());
		}
		
		return path;
	}

	public static void messeage(String text) {
		pNode.log(Status.INFO, text);
	}

}

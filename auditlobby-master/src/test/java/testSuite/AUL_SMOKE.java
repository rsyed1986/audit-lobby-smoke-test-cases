package testSuite;


import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import utility.CoreUtil;
import utility.SeleniumTools;
import utility.TestInit;

public class AUL_SMOKE extends TestInit {

	@BeforeMethod(enabled = true)
	public void beforeclass() {
		try {
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com/auditlobby");
			SeleniumTools.ClearAndSetText("xpath", ".//*[@id='username']", "automation@yopmail.com");
			SeleniumTools.ClearAndSetText("xpath", ".//*[@id='password']", "Abhra@123");
			SeleniumTools.clickOnObject("xpath", "//*[contains(text(),'Login')]");
			SeleniumTools.scrollToElementAndClick("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@AfterMethod
	public void aftermethod() {
		try {
			SeleniumTools.quitBrowser();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Create Project Template Folder")
	public void AUL_TC_01() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Templates')]");
			SeleniumTools.scrollToElementAndClick("xpath", "//button/span[contains(text(),'Create Folder')]");
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='modal-body']/form[@name='ctCtrl.createfolder']/div/input");
			SeleniumTools.ClearAndSetText("xpath",
					"//div[@class='modal-body']/form[@name='ctCtrl.createfolder']/div/input",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='modal-body']/form[@name='ctCtrl.createfolder']/div/button");

		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test(description = "Create Word, Excel, PDF, Audit Program in Project Template Folder")
	public void AUL_TC_02() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Templates')]");
			SeleniumTools.scrollToElementAndClick("xpath", "//div[@class='panel-group']/div[1]");

			SeleniumTools.clickOnObject("xpath",
					"//div[@class='panel-group']/div[1]//div//span[@title='Create New Template']");
			SeleniumTools.clickOnObject("xpath", "//div/form[@name='ctCtrl.createNewTemplate']//input[@name='tname']");
			SeleniumTools.ClearAndSetText("xpath", "//div/form[@name='ctCtrl.createNewTemplate']//input[@name='tname']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//div/form[@name='ctCtrl.createNewTemplate']//button");

			SeleniumTools.scrollToElementAndClick("xpath",
					"//div[@class='panel-group']/div[1]//div[@class='templates-block']//ul/li[1]");
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-6 col-sm-12 folder-layout']//span[text()='New Folder']");
			SeleniumTools.clickOnObject("xpath", "//form[@name='ctCtrl.createNewTempWpFolder']//input[@name='fname']");
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='ctCtrl.createNewTempWpFolder']//input[@name='fname']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath",
					"//form[@name='ctCtrl.createNewTempWpFolder']//button[text()='Create']");

			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-6 col-sm-12 folder-layout']//div[@class='files']/div//div[@class='panel panel-default ng-scope'][1]");
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-6 col-sm-12 folder-layout']//div[@class='panel-heading'][1]//span[@title='Create File']");
			// Excel file creation
			Thread.sleep(1000);
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='ctCtrl.createNewFile']//input[@name='wref']",
					SeleniumTools.getRandomString());
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='ctCtrl.createNewFile']//input[@name='wttl']",
					SeleniumTools.getRandomString());
			SeleniumTools.selectByText("xpath", "//form[@name='ctCtrl.createNewFile']//select[@name='wtype']", "Excel");
			SeleniumTools.clickOnObject("xpath", "//form[@name='ctCtrl.createNewFile']//button[text()='Create']");
			Thread.sleep(3000);
			// Pdf file Creation
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-6 col-sm-12 folder-layout']//div[@class='panel-heading'][1]//span[@title='Create File']");
			Thread.sleep(1000);
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='ctCtrl.createNewFile']//input[@name='wref']",
					SeleniumTools.getRandomString());
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='ctCtrl.createNewFile']//input[@name='wttl']",
					SeleniumTools.getRandomString());
			SeleniumTools.selectByText("xpath", "//form[@name='ctCtrl.createNewFile']//select[@name='wtype']", "Pdf");
			SeleniumTools.clickOnObject("xpath", "//form[@name='ctCtrl.createNewFile']//button[text()='Create']");
			Thread.sleep(3000);
			// Word file creation
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-6 col-sm-12 folder-layout']//div[@class='panel-heading'][1]//span[@title='Create File']");
			Thread.sleep(1000);
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='ctCtrl.createNewFile']//input[@name='wref']",
					SeleniumTools.getRandomString());
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='ctCtrl.createNewFile']//input[@name='wttl']",
					SeleniumTools.getRandomString());
			SeleniumTools.selectByText("xpath", "//form[@name='ctCtrl.createNewFile']//select[@name='wtype']", "Word");
			SeleniumTools.clickOnObject("xpath", "//form[@name='ctCtrl.createNewFile']//button[text()='Create']");
			Thread.sleep(3000);
			// Audit Program creation
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-6 col-sm-12 folder-layout']//div[@class='panel-heading'][1]//span[@title='Create File']");
			Thread.sleep(1000);
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='ctCtrl.createNewFile']//input[@name='wref']",
					SeleniumTools.getRandomString());
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='ctCtrl.createNewFile']//input[@name='wttl']",
					SeleniumTools.getRandomString());
			SeleniumTools.selectByText("xpath", "//form[@name='ctCtrl.createNewFile']//select[@name='wtype']",
					"Audit Program");
			SeleniumTools.clickOnObject("xpath", "//form[@name='ctCtrl.createNewFile']//button[text()='Create']");
			Thread.sleep(3000);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}

	@Test(description = "Import document under Word, Excel,PDF, Audit Program")
	public void AUL_TC_03() {
		try {

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Add steps under Audit Program")
	public void AUL_TC_04() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Templates')]");
			// Clicking on first project template folder
			SeleniumTools.clickOnObject("xpath", "//div[@class='panel-group']/div[1]");
			// Clicking on first temple folder
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='panel-group']/div[1]//div[@class='templates-block']//ul/li[1]");
			// Clicking on first folder under first folder
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-6 col-sm-12 folder-layout']//div[@class='files']/div//div[@class='panel panel-default ng-scope'][1]");
			// Clicking on Audit program template
			SeleniumTools.doubleClickOnObject("xpath",
					"//div[@class='col-md-6 col-sm-12 folder-layout']//div[@class='files']/div//div[@class='panel panel-default ng-scope'][1]//div/ul/li/span[text()='AuditProgram'][1]");
			SeleniumTools.switchToFrameByNumber(0);
			SeleniumTools.clickOnObject("xpath", "//span[text()='Add New Step']");
			SeleniumTools.clickOnObject("xpath", "//table[@id='gv_AuditProgram_Temp_DXMainTable']//tr[2]/td[2]");
			SeleniumTools.ClearAndSetText("xpath", "//table[@id='gv_AuditProgram_Temp_DXMainTable']//tr[2]/td[2]",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//span[text()='Save changes']");
			Thread.sleep(2000);
			// Export Audit Program Template
			SeleniumTools.clickOnObject("xpath", "//span[text()='Template']");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Create Workpaper Library Folder")
	public void AUL_TC_05() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Templates')]");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Workpaper Library']");
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create Folder']");
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='modal-body']/form[@name='wtCtrl.createNewFolder']/div/input");
			SeleniumTools.ClearAndSetText("xpath",
					"//div[@class='modal-body']/form[@name='wtCtrl.createNewFolder']/div/input",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='modal-body']/form[@name='wtCtrl.createNewFolder']/div/button");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Create Audit Program and export and import template with data")
	public void AUL_TC_06() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Templates')]");
			// Clicking on first project template folder
			SeleniumTools.clickOnObject("xpath", "//div[@class='panel-group']/div[1]");
			// Clicking on first temple folder
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='panel-group']/div[1]//div[@class='templates-block']//ul/li[1]");
			// Clicking on first folder under first folder
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-6 col-sm-12 folder-layout']//div[@class='files']/div//div[@class='panel panel-default ng-scope'][1]");
			// Clicking on Audit program template
			SeleniumTools.doubleClickOnObject("xpath",
					"//div[@class='col-md-6 col-sm-12 folder-layout']//div[@class='files']/div//div[@class='panel panel-default ng-scope'][1]//div/ul/li/span[text()='AuditProgram'][1]");
			SeleniumTools.switchToFrameByNumber(0);
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//input[@id='fileuploadExcel_TextBox0_FakeInput']");
			Runtime.getRuntime().exec(
					"E:\\soft\\Subbaraju-workspace\\SOXLOBBY\\src\\test\\resources\\AutIT Scite\\AuditprogramTemplate.exe");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("id", "btnImport_CD");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Create upload file and upload word/excel/PDF")
	public void AUL_TC_07() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Templates')]");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Workpaper Library']");
			SeleniumTools.clickOnObject("xpath", "//div[@class='panel parentAccord ng-scope'][1]");
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='panel parentAccord ng-scope'][1]//span[@title=\"Create New Template\"]");
			SeleniumTools.clickOnObject("xpath", "//form[@name='wtCtrl.createNewTemplate']//input[@name='tname']");
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='wtCtrl.createNewTemplate']//input[@name='tname']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='wtCtrl.createNewTemplate']//button[text()='Create']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='panel parentAccord ng-scope'][1]//div[@class='col-md-12 no-gutters']//ul/li[1]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.switchToFrameByNumber(0);
			SeleniumTools.clickOnObject("xpath", "//input[@id='ASPxUploadControl1_TextBox0_FakeInput']");
			Runtime.getRuntime()
					.exec("E:\\soft\\Subbaraju-workspace\\SOXLOBBY\\src\\test\\resources\\AutIT Scite\\PBCListPdf.exe");
			CoreUtil.imSleepy(5000);
			SeleniumTools.clickOnObject("xpath", "//input[@id='ASPxUploadControl1_TextBox0_FakeInput']");
			Runtime.getRuntime()
					.exec("E:\\soft\\Subbaraju-workspace\\SOXLOBBY\\src\\test\\resources\\AutIT Scite\\ExcelXlsx.exe");
			CoreUtil.imSleepy(5000);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Create Group,Client,User (Confirm the new user)")
	public void AUL_TC_08() {
		try {

			SeleniumTools.clickOnObject("xpath", "//span[text()='Set Up']");
			// Creating user
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create User']");
			Thread.sleep(2000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='usrName']", SeleniumTools.getRandomString());
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='userId']", SeleniumTools.getRandomString());
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='usrEmail']",
					SeleniumTools.getRandomString() + "@yopmail.com");
			SeleniumTools.selectByText("xpath",
					"//select[@name='asgnrole' and @ng-model='UMCtrl.userFormFields.AccessTypeID']", "Super Admin");
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath", "//button[text()='Create']");
			CoreUtil.imSleepy(10000);
			// Creating Groupadmin

			SeleniumTools.scrollToElementAndClick("xpath", "//div[@class='btn-group ng-scope']/button[2]");
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create']");
			SeleniumTools.scrollToElementAndClick("xpath", "//input[@name='GroupName']");
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='GroupName']", SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//button[text()='Create']");
            CoreUtil.imSleepy(5000);
			// Creating Client

			SeleniumTools.clickOnObject("xpath", "//div[@class='btn-group']//button[text()='Client']");
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create']");
			SeleniumTools.clickOnObject("xpath", "//input[@name='ClientName']");
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='ClientName']", SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//button[text()='Create']");
			CoreUtil.imSleepy(5000);
			// Creating EngagementType

			SeleniumTools.clickOnObject("xpath", "//div[@class='btn-group']//button[text()='Engagement Type']");
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create']");
			SeleniumTools.clickOnObject("xpath", "//input[@name='toeName']");
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='toeName']", SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//button[text()='Create']");
			Thread.sleep(3000);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Map Client and User to Group")
	public void AUL_TC_09() {
		try {

			SeleniumTools.clickOnObject("xpath", "//span[text()='Set Up']");
			SeleniumTools.clickOnObject("xpath", "//div[@class='btn-group ng-scope']/button[2]");
			SeleniumTools.clickOnObject("xpath", "//div[@class='ui-grid-canvas']/div[1]/div/div[8]//a[1]");
			// Assign User to group
			SeleniumTools.clickOnObject("xpath", "//div[@class=\"modal-body\"]//div[@class='col-md-5'][1]//ul/li[1]");
			SeleniumTools.clickOnObject("xpath", "//div[@class='col-md-2 arrows']/button[1]");

			// Assign Clients to group
			SeleniumTools.clickOnObject("xpath", "//div[@class='form-group']//label[2]/input");
			SeleniumTools.clickOnObject("xpath", "//div[@class=\"col-md-5\"][1]//ul/li[1]");
			SeleniumTools.clickOnObject("xpath", "//div[@class='col-md-2 arrows']/button[1]");

		} catch (Exception e) {

		}
	}

	@Test(description = "Create Cabinet in Group")
	public void AUL_TC_10() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");

			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			CoreUtil.imSleepy(3000);
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create Cabinet']");
			SeleniumTools.scrollToElementAndClick("xpath", "//input[@name='cabName']");
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='cabName']", SeleniumTools.getRandomString());
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath", "//button[text()='Create']");
			CoreUtil.imSleepy(3000);

		} catch (Exception e) {

		}
	}

	@Test(description = "Create a blank project and Edit the Project Information")
	public void AUL_TC_11() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create Project']");

			// Selecting Client Name
			SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='clientName']",
					1);
			// Selecting Engagement type
			SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='tOE']", 1);
			// Selecting Engagement partner role
			SeleniumTools.selectByTextByIndex("xpath",
					"//div[@class='container-fluid']//select[@name='EngPartnerRole']", 1);
			// Data filling in project name
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='nameAsLink']",
					SeleniumTools.getRandomString());
			// Data filling ProjectID
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='clientId']",
					SeleniumTools.getRandomString());
			// Data filling Fee
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='fee']",
					SeleniumTools.getRandomNumbers());
			// Data filling Address
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='address']",
					SeleniumTools.getRandomString());
			// selecting date Dec 31
			SeleniumTools.clickOnObject("xpath", "//div[@class='container-fluid']//input[@name='fiscalYear']");
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath",
					"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[3]/td[4]");
			SeleniumTools.clickOnObject("xpath",
					"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[6]/td[2]");

			SeleniumTools.clickOnObject("xpath", "//span[text()='Next step']");

			SeleniumTools.clickOnObject("xpath", "//span[text()='Create']");
			CoreUtil.imSleepy(2000);
		} catch (Exception e) {

		}
	}

	@Test(description = "Create project using template which is created before and observe files and data is visible")
	public void AUL_TC_12() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create Project']");

			// Selecting Client Name
			SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='clientName']",
					1);
			// Selecting Engagement type
			SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='tOE']", 1);
			// Selecting Engagement partner role
			SeleniumTools.selectByTextByIndex("xpath",
					"//div[@class='container-fluid']//select[@name='EngPartnerRole']", 1);
			// Data filling in project name
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='nameAsLink']",
					SeleniumTools.getRandomString());
			// Data filling ProjectID
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='clientId']",
					SeleniumTools.getRandomString());
			// Data filling Fee
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='fee']",
					SeleniumTools.getRandomNumbers());
			// Data filling Address
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='address']",
					SeleniumTools.getRandomString());
			// selecting date Dec 31
			SeleniumTools.clickOnObject("xpath", "//div[@class='container-fluid']//input[@name='fiscalYear']");
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath",
					"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[3]/td[4]");
			SeleniumTools.clickOnObject("xpath",
					"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[6]/td[2]");

			SeleniumTools.clickOnObject("xpath", "//span[text()='Next step']");
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-4  col-sm-4']//div[@id='templatechoose']/div[1]");
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-4  col-sm-4']//div[@id='templatechoose']/div[1]//ul/li[1]//input");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Create']");
			CoreUtil.imSleepy(2000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(description = "Create word/excel/pdf files in the same project and then import files which has data (In both Permanent and Workpaper files)", enabled = true)
	public void AUL_TC_13_1() {
		try {

			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Groups')]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[2]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Workpaper File')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Create')]");
			SeleniumTools.clickOnObject("xpath", "//a[contains(text(),'New Folder')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='wCtrl.createfolder']//input",
					SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.createfolder']//button");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//div[@id='parentAccord']/div[1]/div[1]/div");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='wCtrl.fileForm']//select", "Excel");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='wCtrl.fileForm']//select", "Word");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='wCtrl.fileForm']//select", "Pdf");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.fileForm']//div/input[@type='file']");
			CoreUtil.imSleepy(500);
			Runtime.getRuntime().exec("E:\\soft\\Subbaraju-workspace\\AuditLobby\\src\\test\\resources\\AutIT Scite\\PBCListPdf.exe");
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-plus']");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='wCtrl.Childfolder']//input",
					SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.Childfolder']//button");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//div[@id='child2591']/div/div[1]/div");
			SeleniumTools.clickOnObject("xpath", "//div[@id='child2591']//i[@class='fa fa-file']");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='wCtrl.fileForm']//select", "Excel");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='child2591']//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='wCtrl.fileForm']//select", "Word");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='child2591']//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='wCtrl.fileForm']//select", "Pdf");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.fileForm']//div/input[@type='file']");
			CoreUtil.imSleepy(500);
			Runtime.getRuntime().exec("E:\\soft\\Subbaraju-workspace\\AuditLobby\\src\\test\\resources\\AutIT Scite\\PBCListPdf.exe");
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='child2591']//i[@class='fa fa-file']");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='wCtrl.fileForm']//input",
					SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='wCtrl.fileForm']//select", "Audit Program");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.fileForm']//button");
			

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	
	@Test(description = "Create word/excel/pdf files in the same project and then import files which has data (In both Permanent and Workpaper files", enabled = true)
	public void AUL_TC_13_2() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Groups')]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[2]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
		
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Permanent File')]");
		
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Create')]");
			SeleniumTools.clickOnObject("xpath", "//a[contains(text(),'New Folder')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='pFCtrl.createfolder']//input",
					SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//form[@name='pFCtrl.createfolder']//button");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//div[@id='parentAccord']/div[1]/div[1]/div");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='pFCtrl.fileForm']//select", "Excel");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='pFCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='pFCtrl.fileForm']//select", "Word");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='pFCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='pFCtrl.fileForm']//select", "Pdf");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//form[@name='pFCtrl.fileForm']//div/input[@type='file']");
			CoreUtil.imSleepy(500);
			Runtime.getRuntime().exec("E:\\soft\\Subbaraju-workspace\\AuditLobby\\src\\test\\resources\\AutIT Scite\\PBCListPdf.exe");
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='pFCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-plus']");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//form[@name='pFCtrl.Childfolder']//input",
					SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//form[@name='pFCtrl.Childfolder']//button");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//div[@id='child257']/div/div[1]/div");
			SeleniumTools.clickOnObject("xpath", "//div[@id='child257']//i[@class='fa fa-file']");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='pFCtrl.fileForm']//select", "Excel");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='pFCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='child257']//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='pFCtrl.fileForm']//select", "Word");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='pFCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='child257']//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='pFCtrl.fileForm']//select", "Pdf");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//form[@name='pFCtrl.fileForm']//div/input[@type='file']");
			CoreUtil.imSleepy(500);
			Runtime.getRuntime().exec("E:\\soft\\Subbaraju-workspace\\AuditLobby\\src\\test\\resources\\AutIT Scite\\PBCListPdf.exe");
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='pFCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	
	@Test(description = "Create a project using rollforward option(Use the aboveproject)  without content and observe the data in WP files should be balnk and able to import new data and Permanent files data should be visible (same as previous project)")
	public void AUL_TC_14() {
		try {

			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create Project']");
			// Selecting Client Name
			SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='clientName']",
					1);
			// Selecting Engagement type
			SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='tOE']", 1);
			// Selecting Engagement partner role
			SeleniumTools.selectByTextByIndex("xpath",
					"//div[@class='container-fluid']//select[@name='EngPartnerRole']", 1);
			// Data filling in project name
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='nameAsLink']",
					SeleniumTools.getRandomString());
			// Data filling ProjectID
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='clientId']",
					SeleniumTools.getRandomString());
			// Data filling Fee
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='fee']",
					SeleniumTools.getRandomNumbers());
			// Data filling Address
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='address']",
					SeleniumTools.getRandomString());
			// selecting date Dec 31
			SeleniumTools.clickOnObject("xpath", "//div[@class='container-fluid']//input[@name='fiscalYear']");
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath",
					"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[3]/td[4]");
			SeleniumTools.clickOnObject("xpath",
					"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[6]/td[2]");

			SeleniumTools.clickOnObject("xpath", "//span[text()='Next step']");
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-4 col-sm-4 roll-align']//input[@name='choose_template']");
			Thread.sleep(1000);

			SeleniumTools.clickOnObject("xpath", "//span[text()='Create']");
			CoreUtil.imSleepy(2000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Create a project using rollforward option(Use the above project)  with content and observe the data in WP and Permanent files data is visible (same as previous project)")
	public void AUL_TC_15() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='Create Project']");

			// Selecting Client Name
			SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='clientName']",
					1);
			// Selecting Engagement type
			SeleniumTools.selectByTextByIndex("xpath", "//div[@class='container-fluid']//select[@name='tOE']", 1);
			// Selecting Engagement partner role
			SeleniumTools.selectByTextByIndex("xpath",
					"//div[@class='container-fluid']//select[@name='EngPartnerRole']", 1);
			// Data filling in project name
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='nameAsLink']",
					SeleniumTools.getRandomString());
			// Data filling ProjectID
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='clientId']",
					SeleniumTools.getRandomString());
			// Data filling Fee
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='fee']",
					SeleniumTools.getRandomNumbers());
			// Data filling Address
			SeleniumTools.ClearAndSetText("xpath", "//div[@class='container-fluid']//input[@name='address']",
					SeleniumTools.getRandomString());
			// selecting date Dec 31
			SeleniumTools.clickOnObject("xpath", "//div[@class='container-fluid']//input[@name='fiscalYear']");
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath",
					"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[3]/td[4]");
			SeleniumTools.clickOnObject("xpath",
					"//div[@id='createCabinetOrClient']/div/div/div[2]/div/form/div/ng-include/div/div/div/div/div/div[4]/div/div/div/table/tbody/tr[6]/td[2]");

			SeleniumTools.clickOnObject("xpath", "//span[text()='Next step']");
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='col-md-4 col-sm-4 roll-align']//input[@name='choose_template']");
			Thread.sleep(1000);
			SeleniumTools.clickOnObject("xpath", "//input[@type='checkbox']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[text()='Create']");
			CoreUtil.imSleepy(2000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(description = "Create Entity and upload trial balance and create adjustment", enabled = true)
	public void AUL_TC_16() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.clickOnObject("xpath",
					"//span[@class='sidenav-label ng-scope'][contains(text(),'Financial Statements')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Trial Balance')]");
			SeleniumTools.switchToFrameByNumber(0);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='txtTB']", SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Create Entity')]");
			SeleniumTools.clickOnObject("id", "fileuploadExcel_TextBox0_FakeInput");
			CoreUtil.imSleepy(2000);
			//Require xls sample trail balance sheet
			Runtime.getRuntime().exec("E:\\soft\\Subbaraju-workspace\\AuditLobby\\src\\test\\resources\\AutIT Scite\\Trail.exe");
			CoreUtil.imSleepy(3000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Import Data')]");
			SeleniumTools.switchToDefaultFrame();
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Adjustments')]");
			SeleniumTools.clickOnObject("xpath", "//button/span[contains(text(),'Create Adjustment')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='adjname']", SeleniumTools.getRandomString());
			SeleniumTools.selectByText("xpath", "//form[@name='aCtrl.adjForm']//select[@name='adjtype']", "Audit");
			SeleniumTools.selectByText("xpath", "//form[@name='aCtrl.adjForm']//select[@name='wref']", "1-1");
			SeleniumTools.clickOnObject("xpath", "//button[contains(text(),'Submit')]");
			CoreUtil.imSleepy(1000);
		    //In the above line view button n ot working	
			SeleniumTools.clickOnObject("xpath", "//div[@class='ui-grid-canvas']/div[1]/div//span/i[@title='View']");
			
			CoreUtil.imSleepy(3000);
			//SeleniumTools.switchToFrameByNumber(0);
			//SeleniumTools.scrollToElementAndClick("xpath", "//span[contains(text(),'ADD')]");
			//SeleniumTools.clickOnObject("xpath", "//table[@id='ASPxGridView1_DXEditor0']/tbody/tr/td[2]/input");
			
			
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Test(description = "Add users to project")
	public void AUL_TC_17() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Administration']");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Users']");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Assign User']");
			SeleniumTools.selectByTextByIndex("xpath", "//form[@name='uCtrl.userAssignForm']//select[@name='asgnmail']",
					1);
			SeleniumTools.selectByTextByIndex("xpath", "//form[@name='uCtrl.userAssignForm']//select[@name='asgnrole']",
					2);
			SeleniumTools.clickOnObject("xpath", "//button[text()='Assign']");
			Thread.sleep(7000);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Add time line item and PBC item as partner")
	public void AUL_TC_18() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");

			// **************Workpaper timeline ***********************
			SeleniumTools.clickOnObject("xpath", "//span[text()='Workpaper Timeline']");
			CoreUtil.imSleepy(3000);
			SeleniumTools.switchToFrameByNumber(0);
			SeleniumTools.scrollToElementAndClick("xpath", "//span[text()='ADD']");
			// Adding description
			SeleniumTools.ClearAndSetText("id", "timline_grid_DXEditor2_I", SeleniumTools.getRandomString());
			// Clicking on Due date
			SeleniumTools.clickOnObject("xpath", "//td[@id='timline_grid_DXEditor4_B-1']");
			SeleniumTools.clickOnObject("xpath", "//td[@id='timline_grid_DXEditor4_DDD_C_NMC']");
			SeleniumTools.clickOnObject("xpath",
					"//td[@class=\"dxeCalendarDay_MetropolisBlue\" and contains(text(),'31')]");
			CoreUtil.imSleepy(2000);
			// Adding # of days Before Due
			SeleniumTools.ClearAndSetText("id", "timline_grid_DXEditor7_I", "22");
			// Adding frequency of Reminder
			SeleniumTools.clickOnObject("id", "timline_grid_DXEditor8_B-1Img");
			SeleniumTools.clickOnObject("xpath", "//td[@id='timline_grid_DXEditor8_DDD_L_LBI0T0']");
			// Clicking on Save changes
			SeleniumTools.clickOnObject("xpath", "//span[text()='Save changes']");
			
			// ***********PBC LIst *****************
			SeleniumTools.switchToDefaultFrame();
			SeleniumTools.clickOnObject("xpath", "//span[text()='PBC List']");
			SeleniumTools.switchToFrameByNumber(1);
			
			SeleniumTools.scrollToElementAndClick("id", "btn_ADD_CD");
			SeleniumTools.ClearAndSetText("xpath", "//table[@id='ASPxGridView1_DXEditor2']//textarea",
					SeleniumTools.getRandomString());
			
			SeleniumTools.clickOnObject("xpath", "//div[2]/table/tbody/tr[2]/td[4]");
			SeleniumTools.ClearAndSetText("xpath", "//td[4]/div[2]/table/tbody/tr/td/textarea",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//tr[@id='ASPxGridView1_DXDataRow-1']/td[6]/div");
			SeleniumTools.clickOnObject("xpath", "//td[@id='ASPxGridView1_DXEditor5_B-1']/img");
			SeleniumTools.clickOnObject("id", "ASPxGridView1_DXEditor5_DDD_C_BT");
			SeleniumTools.clickOnObject("xpath", "//tr[@id='ASPxGridView1_DXDataRow-1']/td[7]/div");
			
			SeleniumTools.ClearAndSetText("xpath", "//table[@id='ASPxGridView1_DXEditor7']/tbody/tr/td/input", "1");
			
			SeleniumTools.clickOnObject("xpath", "//tr[@id='ASPxGridView1_DXDataRow-1']/td[8]");
			
			SeleniumTools.ClearAndSetText("xpath", "//table[@id='ASPxGridView1_DXEditor8']/tbody/tr/td/input", "1");
			
			SeleniumTools.clickOnObject("xpath", "//tr[@id='ASPxGridView1_DXDataRow-1']/td[9]");
			
			SeleniumTools.ClearAndSetText("xpath", "//table[@id='ASPxGridView1_DXEditor9']/tbody/tr/td/input", "1");
			
			SeleniumTools.clickOnObject("xpath", "//tr[@id='ASPxGridView1_DXDataRow-1']/td[10]/div");
			
			SeleniumTools.clickOnObject("xpath", "//table[2]/tbody/tr/td[3]/img");
			
			SeleniumTools.clickOnObject("xpath", "//table[2]/tr/td");
			
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Save changes')]");
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Signoff the Workpaper as partner and also open any workpaper file (close the WP using browser close option without clicking on Close button)")
	public void AUL_TC_19() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='custom-scrollbar']//span[contains(text(),'Workpaper File')]");
			SeleniumTools.clickOnObject("xpath", "//div[2]/div/div/div/span");
			// SeleniumTools.clickOnObject("xpath", "//div[@class='panel-body
			// ng-isolate-scope']//ul/li[1]");
			// File creating under Workpaper folder
			/*
			 * SeleniumTools.clickOnObject("xpath",
			 * "//div[@class='panel-title collapseHandler ng-isolate-scope']/div//span[@title='Create File']"
			 * ); CoreUtil.imSleepy(5000); SeleniumTools.ClearAndSetText("xpath",
			 * "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			 * SeleniumTools.selectByTextByIndex("xpath", "//select[@name='newfilewtype']",
			 * 6); SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
			 * SeleniumTools.getRandomString()); CoreUtil.imSleepy(2000);
			 * SeleniumTools.clickOnObject("xpath",
			 * "//button[text()='Create' and @ng-click='wCtrl.createFile()']");
			 */
			CoreUtil.imSleepy(2000);
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='panel-body ng-isolate-scope']//ul/li[1]");
			CoreUtil.imSleepy(5000);
			SeleniumTools.switchToChildWindow();
			CoreUtil.imSleepy(2000);
			SeleniumTools.switchToMainWindow();
			SeleniumTools.clickOnObject("xpath", "//span[@title='Notifications']");
			SeleniumTools.getText("xpath", "//div[@class=\"col-md-8\"]//span/b");
			SeleniumTools.getText("xpath", "//div[@class='col-md-12 ng-binding']");
			CoreUtil.imSleepy(2000);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Create review, followup comment and signoff the same")
	public void AUL_TC_20() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='custom-scrollbar']//span[contains(text(),'Workpaper File')]");

			// *************************Creating Follow up comment************************
			SeleniumTools.clickOnObject("xpath", "//div[2]/div/div/div/span");
			SeleniumTools.clickOnObject("xpath", "//div[@class='panel-body ng-isolate-scope']//ul/li[1]");
			SeleniumTools.clickOnObject("xpath", "//div[@class='file-menu']//span[contains(text(),'Create')]");
			SeleniumTools.clickOnObject("xpath", "//a[contains(text(),'Add Follow Up Comment')]");
			CoreUtil.imSleepy(3000);
			SeleniumTools.selectByTextByIndex("xpath", "//select[@name='priority']", 1);
			SeleniumTools.clickOnObject("xpath", "//button[@type='button' and contains(text(),'Select')]");
			SeleniumTools.clickOnObject("xpath", "//ul[@class='dropdown-menu dropdown-menu-form ng-scope']//li[4]");
			SeleniumTools.clickOnObject("xpath", "//div[@class='multiselect-parent btn-group dropdown-multiselect']");
			SeleniumTools.ClearAndSetText("xpath", "//textarea[@name='cmt']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.ReviewForm']//button[contains(text(),'Create')]");
			CoreUtil.imSleepy(10000);
			// ***************Create Review comment*****************
			// SeleniumTools.clickOnObject("xpath", "//div[@class='panel-body
			// ng-isolate-scope']//ul/li[1]");
			SeleniumTools.clickOnObject("xpath", "//div[@class='file-menu']//span[contains(text(),'Create')]");
			SeleniumTools.clickOnObject("xpath", "//a[contains(text(),'Add Review Comment')]");
			CoreUtil.imSleepy(3000);
			SeleniumTools.selectByTextByIndex("xpath", "//select[@name='priority']", 1);
			SeleniumTools.clickOnObject("xpath", "//button[@type='button' and contains(text(),'Select')]");
			SeleniumTools.clickOnObject("xpath", "//ul[@class='dropdown-menu dropdown-menu-form ng-scope']//li[4]");
			SeleniumTools.clickOnObject("xpath", "//div[@class='multiselect-parent btn-group dropdown-multiselect']");
			SeleniumTools.ClearAndSetText("xpath", "//textarea[@name='cmt']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.ReviewForm']//button[contains(text(),'Create')]");

			CoreUtil.imSleepy(10000);
			SeleniumTools.clickOnObject("xpath", "//a[contains(text(),'Review Comments')]");
			SeleniumTools.clickOnObject("xpath", "//div[@class='ui-grid-canvas']/div[1]");
			SeleniumTools.clickOnObject("xpath", "//button[contains(text(),'Sign Off')]");
			CoreUtil.imSleepy(5000);

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	@Test(description = "Verify the client Dashboard")
	public void AUL_TC_21() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath",
					"//div[@class='custom-scrollbar']//span[contains(text(),'Dashboard')]");
			SeleniumTools.fetchingTableDataUsingTRAndTDTags("xpath",
					"/html/body/div[2]/div/div[4]/div/div/div/scrollable-tabset/div/div[1]/div/div/div[1]/div/div/div/div[3]/div/div[2]/table");

		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(description = "Create Leadsheet workpaper and link WP references", enabled = true)
	public void AUL_TC_26() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Groups')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Workpaper File')]");
		
		
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='parentAccord']/div[1]/div[1]/div");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-file']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewref']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.selectByText("xpath", "//form[@name='wCtrl.fileForm']//select", "Lead Sheet");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='newfilewtitle']",
					SeleniumTools.getRandomString());
			SeleniumTools.clickOnObject("xpath", "//form[@name='wCtrl.fileForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.switchToFrameByNumber(0);
			SeleniumTools.clickOnObject("xpath", "//tr[@id='grvLeadSheet_DXDataRow1']/td[4]");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//td[@id='grvLeadSheet_DXEditor4_B-1']/img");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//table[@id='grvLeadSheet_DXEditor4_DDD_L_LBT']/tr[4]/td");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Save changes')]");
			CoreUtil.imSleepy(2000);

			SeleniumTools.switchToDefaultFrame();
			SeleniumTools.clickOnObject("xpath",
					"//span[@class='sidenav-label ng-scope'][contains(text(),'Financial Statements')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Lead Sheets')]");

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	
	@Test(description = "Create WP file using Templates", enabled = true)
	public void AUL_TC_33() {
		try {

			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Groups')]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Workpaper File')]");
			

			SeleniumTools.clickOnObject("xpath", "//a[contains(text(),'Expand All')]");
			
			SeleniumTools.clickOnObject("xpath", "//a[contains(text(),'Templates')]");
			
			SeleniumTools.selectByText("xpath", "//div[3]/form[@class='ng-pristine ng-valid']//select[1]",
					"WFVAJVNLCZ");
			
			SeleniumTools.selectByText("xpath", "//div[3]//form//select[2]", "FKZDZGEGTA");
			
			SeleniumTools.dragAndDrop("//div/ul[@class='dragTemps listscroll']/li[4]",
					"//div[@class='panel-body ng-isolate-scope'][1]//div[@ng-if='category.fileslist']");
			// SeleniumTools.ClearAndSetText("xpath",
			// "//form[@name='wCtrl.createTempByDragnDrop']//input[@name='Wref']",
			// SeleniumTools.getRandomString());
			
			// SeleniumTools.clickOnObject("xpath",
			// "//form[@name='wCtrl.createTempByDragnDrop']//button");

		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Test(description = "FS setup and Financial statement/Consolidation view", enabled = true)
	public void AUL_TC_37() {
		try {
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Groups')]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			
			SeleniumTools.clickOnObject("xpath", "//span[@class='sidenav-label ng-scope'][contains(text(),'Financial Statements')]");
			SeleniumTools.clickOnObject("xpath", "//span[@class='side-sub-menu ng-scope'][contains(text(),'FS Set Up')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@class='col-md-3 col-sm-6']//span[contains(text(),'Balance Sheet')]/parent::*//following-sibling::span/i");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//input[@name='fsgrpanme']", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//form[@name='SuCtrl.addFSGroupForm']//button");
			SeleniumTools.clickOnObject("xpath", "//div[@class='col-md-9 no-gutters']/span");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-arrow-circle-right fa-2x']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='addFStype']//i[@class='fa fa-window-close']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[2]/div/div[2]/ul/li[1]/span[2]/i[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.ClearAndSetText("xpath", "//div[@id='addFSgroup']//form/div/input", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//form[@name='SuCtrl.addGrpForm']//button");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='addFSgroup']//li/div[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-arrow-circle-right fa-2x']");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//div[@id='addFSgroup']//i[@class='fa fa-window-close']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[3]/div/div[2]/ul/li/span[2]/i");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='addFSsubgroup']//ul/li[1]");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-arrow-circle-right fa-2x']");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//div[@id='addFSsubgroup']//ul/li[1]");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-arrow-circle-right fa-2x']");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//div[@id='addFSsubgroup']//ul/li[1]");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//i[@class='fa fa-arrow-circle-right fa-2x']");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//div[@id='addFSsubgroup']//i[@class='fa fa-window-close']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[@class='side-sub-menu ng-scope'][contains(text(),'Financial Statements')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Consolidating View')]");
			CoreUtil.imSleepy(1000);

		} catch (Exception e) {
			e.printStackTrace();

		}

	}



	@Test(description = "Archive Workpaper file by meeting all the conditions", enabled = true)
	public void AUL_TC_38() {
		try {

			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Groups')]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Workpaper File')]");
			CoreUtil.imSleepy(5000);
			SeleniumTools.scrollToElementAndClick("xpath", "//a[contains(text(),'Archive')]");
			CoreUtil.imSleepy(5000);
			SeleniumTools.getAlertText();
			
			
			
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}

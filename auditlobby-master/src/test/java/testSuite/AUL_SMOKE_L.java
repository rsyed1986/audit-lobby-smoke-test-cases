package testSuite;

import org.testng.annotations.Test;

import utility.CoreUtil;
import utility.SeleniumTools;

public class AUL_SMOKE_L {
	@Test(description = "Login as Client Access and add one record and upload multiple documents under both records, Able to view the data after upload")
	public void AUL_TC_22() {
		try {
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com");
			SeleniumTools.ClearAndSetText("id", "username", "clientaccess@yopmail.com");
			SeleniumTools.ClearAndSetText("id", "password", "Client@123");
			SeleniumTools.clickOnObject("xpath", "//button[text()='Login']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.scrollToElementAndClick("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Groups')]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			CoreUtil.imSleepy(3000);
			SeleniumTools.switchToFrameByNumber(0);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Add')]");
			CoreUtil.imSleepy(1500);
			SeleniumTools.ClearAndSetText("xpath", "//table[@id='ASPxGridView1_DXEditor2']//textarea", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(1500);
			SeleniumTools.clickOnObject("xpath", "//div[2]/table/tbody/tr[2]/td[4]");
			SeleniumTools.ClearAndSetText("xpath", "//td[4]/div[2]/table/tbody/tr/td/textarea", SeleniumTools.getRandomString());
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//tr[@id='ASPxGridView1_DXDataRow-1']/td[6]/div");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//td[@id='ASPxGridView1_DXEditor5_B-1']/img");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//td[@id='ASPxGridView1_DXEditor5_DDD_C_NMC']/img");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//table[@id='ASPxGridView1_DXEditor5_DDD_C_mt']/tbody/tr[7]/td[3]");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//tr[@id='ASPxGridView1_DXDataRow-1']/td[7]/div");
			CoreUtil.imSleepy(1500);
			SeleniumTools.ClearAndSetText("xpath", "//table[@id='ASPxGridView1_DXEditor7']/tbody/tr/td/input", "1");
			CoreUtil.imSleepy(1500);
			SeleniumTools.clickOnObject("xpath", "//tr[@id='ASPxGridView1_DXDataRow-1']/td[8]");
			
			SeleniumTools.ClearAndSetText("xpath", "//table[@id='ASPxGridView1_DXEditor8']/tbody/tr/td/input", "1");
			
			SeleniumTools.clickOnObject("xpath", "//tr[@id='ASPxGridView1_DXDataRow-1']/td[9]");
			
			SeleniumTools.ClearAndSetText("xpath", "//table[@id='ASPxGridView1_DXEditor9']/tbody/tr/td/input", "1");
			
			SeleniumTools.clickOnObject("xpath", "//tr[@id='ASPxGridView1_DXDataRow-1']/td[10]/div");
			
			SeleniumTools.clickOnObject("xpath", "//td[@id='ASPxGridView1_DXEditor11_B-1']/img");
			
			SeleniumTools.clickOnObject("xpath", "//table[@id='ASPxGridView1_DXEditor11_DDD_L_LBT']/tr[4]/td");
			CoreUtil.imSleepy(4000);
			SeleniumTools.clickOnObject("xpath", "//span[text()='Save changes']");
			
			SeleniumTools.doubleClickOnObject("xpath", "//a[@id='ASPxGridView1_cell1_14_UploadBtn']");
			
			SeleniumTools.clickOnObject("xpath", "//td[@id='ASPxUploadControl1_Browse0']/a");
			
			Runtime.getRuntime().exec("E:\\soft\\Subbaraju-workspace\\SOXLOBBY\\src\\test\\resources\\AutIT Scite\\PBCListPdf.exe");
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Send')]");
			CoreUtil.imSleepy(3000);
			
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Test(description = "Login as Partner and verify the client access user uploaded documents, Also able to delete the uploaded documents")
	public void AUL_TC_23() {
		try {
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com");
			SeleniumTools.ClearAndSetText("id", "username", "partnerabhra@yopmail.com");
			SeleniumTools.ClearAndSetText("id", "password", "Partner@123");
			SeleniumTools.clickOnObject("xpath", "//button[text()='Login']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.scrollToElementAndClick("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'PBC List')]");
			CoreUtil.imSleepy(10000);
			SeleniumTools.switchToFrameByNumber(0);
			SeleniumTools.clickOnObject("xpath", "//td[@id='ASPxGridView1_tccell1_14']/a");
			CoreUtil.imSleepy(5000);
			SeleniumTools.clickOnObject("xpath", "//td/a[@id='LinkButton1']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//td[@id='ASPxGridView1_tccell1_18']//a");
			CoreUtil.imSleepy(2000);
			//SeleniumTools.clickOnObject("xpath", "//span[text()='Delete All']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.getAlertText();
			CoreUtil.imSleepy(2000);
		} catch (Exception e) {
			e.printStackTrace();
		}

	}
	
	@Test(description="Sign off as QC and observe the QC status report")
	public void AUL_TC_24() {
		try {
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com");
			SeleniumTools.ClearAndSetText("id", "username", "qualitycontrolabhra@yopmail.com");
			SeleniumTools.ClearAndSetText("id", "password", "Quality@123");
			SeleniumTools.clickOnObject("xpath", "//button[text()='Login']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.scrollToElementAndClick("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Groups']");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[text()='Workpaper File']");
			SeleniumTools.clickOnObject("xpath", "//span[text()='Actions']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//a[text()='QC View']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//div[@class='panel-body ng-scope']//ul/li[1]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//a[text()='QC Review']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.getAlertText();
			SeleniumTools.clickOnObject("xpath", "//div[@class='global-header-left FRL']/a[1]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//span[text()='QC Status Report']");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	@Test(description = "Sign off as QC and observe the QC status report", enabled = true)
	public void AUL_TC_25() {
		try {
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com");
			SeleniumTools.ClearAndSetText("id", "username", "qualitycontrolabhra@yopmail.com");
			SeleniumTools.ClearAndSetText("id", "password", "Quality@123");
			SeleniumTools.clickOnObject("xpath", "//button[text()='Login']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.scrollToElementAndClick("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Recently Visited Projects')]");

			SeleniumTools.clickOnObject("xpath", "//div[@id='group']/ul/li");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@id='group']/ul/li");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Workpaper File')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Testing ')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Excel')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.mouseLeftClick("xpath", "//span[contains(text(),'Excel')]");
			Thread.sleep(2000);
	        SeleniumTools.clickOnObject("xpath", "//div[@class='dropdown position-fixed contxtMenu ng-isolate-scope open']//li[3]/a");
	        SeleniumTools.getAlertText();
	        SeleniumTools.mouseLeftClick("xpath", "//span[contains(text(),'Excel')]");
	        CoreUtil.imSleepy(500);
	        SeleniumTools.clickOnObject("xpath", "//a[contains(text(),'QC Review')]");
	        CoreUtil.imSleepy(1000);
	        SeleniumTools.getAlertText();
	        SeleniumTools.clickOnObject("xpath", "//a[@class='navbar-brand navbar-brand-center']");
	        CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'QC Status Report')]");
			
			
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

	@Test(description = "Login as staff and powersignoff the Workpaper files", enabled = true)
	public void AUL_TC_27() {

		{
			try {
				SeleniumTools.openBrowser("chrome");
				SeleniumTools.navigateURL("https://abhrainc.auditlobby.com");
				SeleniumTools.ClearAndSetText("id", "username", "staffabhra@yopmail.com");
				SeleniumTools.ClearAndSetText("id", "password", "Staff@123");
				SeleniumTools.clickOnObject("xpath", "//button[text()='Login']");
				CoreUtil.imSleepy(2000);
				SeleniumTools.scrollToElementAndClick("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
				CoreUtil.imSleepy(2000);

				SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Groups')]");
				SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']//ul/li[1]");
				SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
				SeleniumTools.doubleClickOnObject("xpath", "//div[@class='folder-layout']/ul/li[1]");
				SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Workpaper File')]");
				CoreUtil.imSleepy(1000);
				SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Testing ')]");
				CoreUtil.imSleepy(2000);
				SeleniumTools.powerSignoff("//div[@class='panel-body ng-isolate-scope']//ul/li[2]",
						"//div[@class='panel-body ng-isolate-scope']//ul/li[3]");
				SeleniumTools.clickOnObject("xpath", "//div[1]/nav/ul[1]/li[3]/a[contains(text(),'UnReview')]");
				SeleniumTools.getAlertText();
			} catch (Exception e) {
				e.printStackTrace();

			}
		}

	}
	
	@Test(description = "Open the WP file which is opened in TC_19 as partner and observe the file is in readonly mode, Request the Access", enabled = true)
	public void AUL_TC_28() {
		try {
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com");
			SeleniumTools.ClearAndSetText("id", "username", "partnerabhra@yopmail.com");
			SeleniumTools.ClearAndSetText("id", "password", "Partner@123");
			SeleniumTools.clickOnObject("xpath", "//button[text()='Login']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Groups')]");
			SeleniumTools.ClearAndSetText("id", "searchField", " Testing");
			SeleniumTools.clickOnObject("xpath", "//button[@class='btn btn-primary']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.doubleClickOnObject("xpath", "//div[@class='col-md-6 col-sm-6 ng-binding']");
			CoreUtil.imSleepy(1000);
			SeleniumTools.doubleClickOnObject("xpath", "//li[@folder='data']/div[1]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='group']/ul/li");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@id='group']/ul/li");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Workpaper File')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='parentAccord']/div/div[1]/div");
			CoreUtil.imSleepy(3000);
			SeleniumTools.clickOnObject("xpath", "//li[1]/span[6]");
			CoreUtil.imSleepy(3000);
			SeleniumTools.doubleClickOnObject("xpath", "//li[1]/span[6]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.switchToChildWindow();
			CoreUtil.imSleepy(3000);
			SeleniumTools.clickOnObject("xpath", "//button/span[contains(text(),'Request Access')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//div[@id='createRqstdiag']//textarea",
					SeleniumTools.getRandomString());
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='createRqstdiag']/div//button[contains(text(),'Submit')]");

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	
@Test(description = "Login as Partner and observe the request in Notification Menu, then close it", enabled = true)
	public void AUL_TC_29() {
		try {	
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com");
			SeleniumTools.ClearAndSetText("id", "username", "partnerabhra@yopmail.com");
			SeleniumTools.ClearAndSetText("id", "password", "Partner@123");
			SeleniumTools.clickOnObject("xpath", "//button[text()='Login']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//span[@class='fa fa-bell alerted']");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//label[@class='radio-inline rd-btn']/input[@name='files']");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Requested files')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//span[@title='Close']");
			SeleniumTools.getAlertText();

	
		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	
	
	
	@Test(description = "Login as staff again and observe the requested file is in Editable Mode", enabled = true)
	public void AUL_TC_30() {
		try {
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com");
			SeleniumTools.ClearAndSetText("id", "username", "staffabhra@yopmail.com");
			SeleniumTools.ClearAndSetText("id", "password", "Staff@123");
			SeleniumTools.clickOnObject("xpath", "//button[text()='Login']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Recently Visited Projects')]");

			SeleniumTools.clickOnObject("xpath", "//div[@id='group']/ul/li");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@id='group']/ul/li");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Workpaper File')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Testing ')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.doubleClickOnObject("xpath", "//div[@id='1091']//ul/li[1]");
			SeleniumTools.switchToChildWindow();
			CoreUtil.imSleepy(1000);
			CoreUtil.imSleepy(3000);
			SeleniumTools.clickOnObject("xpath", "//button/span[contains(text(),'Request Access')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.ClearAndSetText("xpath", "//div[@id='createRqstdiag']//textarea",
					SeleniumTools.getRandomString());
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='createRqstdiag']/div//button[contains(text(),'Submit')]");

		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	

	@Test(description = "Login as Partner and export the Workpaper and Permament files")
	public void AUL_TC_35() {
		try {	
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com");
			SeleniumTools.ClearAndSetText("id", "username", "partnerabhra@yopmail.com");
			SeleniumTools.ClearAndSetText("id", "password", "Partner@123");
			SeleniumTools.clickOnObject("xpath", "//button[text()='Login']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Recently Visited Projects')]");
			SeleniumTools.clickOnObject("xpath", "//div[@id='group']/ul/li");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@id='group']/ul/li");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Workpaper File')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//li/a/span[contains(text(),'Actions')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//li/a[contains(text(),'Export')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.getAlertText();
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//span[@class='fa fa-times close-tab ng-scope']");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Permanent File')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='pm']//a/span[contains(text(),'Actions')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//div[@id='pm']//a[contains(text(),'Export')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.getAlertText();
			CoreUtil.imSleepy(2000);
		
			
		} catch (Exception e) {
			e.printStackTrace();

		}

	}
	
	@Test(description = "Login as Partner and Generate the skeleton of the Workpaper and Permament files")
	public void AUL_TC_36() {
		try {	
			SeleniumTools.openBrowser("chrome");
			SeleniumTools.navigateURL("https://abhrainc.auditlobby.com");
			SeleniumTools.ClearAndSetText("id", "username", "partnerabhra@yopmail.com");
			SeleniumTools.ClearAndSetText("id", "password", "Partner@123");
			SeleniumTools.clickOnObject("xpath", "//button[text()='Login']");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//li[1]/div[3]/button[contains(text(),'View')]");
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//span[contains(text(),'Recently Visited Projects')]");
			SeleniumTools.clickOnObject("xpath", "//div[@id='group']/ul/li");
			SeleniumTools.doubleClickOnObject("xpath", "//div[@id='group']/ul/li");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Workpaper File')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//li/a/span[contains(text(),'Actions')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//li/a[contains(text(),'Generate')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.getAlertText();
			CoreUtil.imSleepy(2000);
			SeleniumTools.clickOnObject("xpath", "//span[@class='fa fa-times close-tab ng-scope']");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//a/span[contains(text(),'Permanent File')]");
			CoreUtil.imSleepy(1000);
			SeleniumTools.clickOnObject("xpath", "//div[@id='pm']//a/span[contains(text(),'Actions')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.clickOnObject("xpath", "//div[@id='pm']//a[contains(text(),'Generate')]");
			CoreUtil.imSleepy(500);
			SeleniumTools.getAlertText();
			CoreUtil.imSleepy(2000);
		
			
		} catch (Exception e) {
			e.printStackTrace();

		}

	}

}

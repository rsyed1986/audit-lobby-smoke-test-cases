package utility;

import java.io.File;
import java.util.List;
import java.util.Random;
import java.util.Set;

import org.apache.commons.io.FileUtils;
import org.apache.log4j.lf5.LogLevel;
import org.openqa.selenium.Alert;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.Keys;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SeleniumTools extends TestInit {

	public static WebDriver driver;
	static WebElement element;
	static Actions action;
	static LoggerUtil loggerUtil = new LoggerUtil(SeleniumTools.class);
	static int screenshotCounter = 1;

	public static void openBrowser(String browserName) throws Exception {
		try {
			if (browserName.equalsIgnoreCase("Firefox")) {

				try {
					loggerUtil.entryLogger("openFirefoxBrowser");
					System.setProperty("webdriver.gecko.driver", "src\\main\\resources\\Drivers\\geckodriver.exe");
					driver = new FirefoxDriver();
					driver.manage().window().maximize();
				} catch (Exception e) {
					loggerUtil.LogSevereMessage(LogLevel.SEVERE, "openFirefoxBrowser", "unable to open firefox browser",
							e);
				} finally {
					loggerUtil.exitLogger("openFirefoxBrowser");
				}

			} else if (browserName.equalsIgnoreCase("chrome")) {
				System.setProperty("webdriver.chrome.driver", "src\\main\\resources\\Drivers\\chromedriver.exe");
				try {
					loggerUtil.entryLogger("openChromeBrowser");
					ChromeOptions options = new ChromeOptions();
					options.addArguments("chrome.switches", "--disable-extensions");
					options.addArguments("disable-infobars");
					driver = new ChromeDriver(options);
					driver.manage().window().maximize();
				} catch (Exception e) {
					loggerUtil.LogSevereMessage(LogLevel.SEVERE, "openChromeBrowser", "unable to open chrome browser",
							e);
				} finally {
					loggerUtil.exitLogger("openChromeBrowser");
				}
			} else if (browserName.equalsIgnoreCase("IE")) {
				try {
					loggerUtil.entryLogger("openIEBrowser");
					System.setProperty("webdriver.ie.driver", "src\\main\\resources\\Drivers\\IEDriverServer.exe");
					DesiredCapabilities capabilities = DesiredCapabilities.internetExplorer();
					capabilities.setCapability(InternetExplorerDriver.INTRODUCE_FLAKINESS_BY_IGNORING_SECURITY_DOMAINS,
							true);
					capabilities.setCapability("requireWindowFocus", true);
					capabilities.setCapability("nativeEvents", false);
					driver = new InternetExplorerDriver();
					driver.manage().window().maximize();
				} catch (Exception e) {
					loggerUtil.LogSevereMessage(LogLevel.SEVERE, "openIEBrowser", "unable to open Internet explorer",
							e);
				} finally {
					loggerUtil.exitLogger("openIEBrowser");
				}
			}
		} catch (WebDriverException e) {
			System.out.println(e.getMessage());
		}
	}

	public static By locatorValue(String locatorTpye, String value) {
		By by;
		switch (locatorTpye) {
		case "id":
			by = By.id(value);
			break;
		case "name":
			by = By.name(value);
			break;
		case "xpath":
			by = By.xpath(value);
			break;
		case "css":
			by = By.cssSelector(value);
			break;
		case "linkText":
			by = By.linkText(value);
			break;
		case "partialLinkText":
			by = By.partialLinkText(value);
			break;
		default:
			by = null;
			break;
		}
		return by;
	}

	public void fillData(String locatorType, String value, String text) {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			loggerUtil.entryLogger("fillDataByID");
			CoreUtil.imSleepy(1000);
			WebElement element = driver.findElement(locator);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			CoreUtil.imSleepy(1000);
			element.sendKeys(text);
			loggerUtil.LogMessage(LogLevel.DEBUG, "fillData", "Data filled: " + text);
		} catch (NoSuchElementException e) {
			try {
				loggerUtil.LogSevereMessage(LogLevel.ERROR, "fillData", "unable to fill data", e);
			} catch (Exception ex) {
				ex.printStackTrace();
			}

		} finally {
			loggerUtil.exitLogger("fillData");
		}
	}

	public static void clickOnObject(String locatorType, String value) throws Exception {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			loggerUtil.entryLogger("clickObject");
			explicitWait(locatorType, value);
			WebElement element = driver.findElement(locator);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			Thread.sleep(500);
			element.click();
			// pNode.info(element.toString());
		} catch (NoSuchElementException e) {
			loggerUtil.LogSevereMessage(LogLevel.ERROR, "clickObject", "unable to click on object", e);
		} finally {
			loggerUtil.exitLogger("clickObject");
		}
	}

	public static void scrollToElementAndClick(String locatorType, String value) throws Exception {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			loggerUtil.entryLogger("scrollToElementAndClick");
			explicitWait(locatorType, value);
			Actions actions = new Actions(driver);
			actions.moveToElement(driver.findElement((locator))).click().build().perform();

		} catch (NoSuchElementException e) {
			loggerUtil.LogSevereMessage(LogLevel.ERROR, "clickObject", "unable to click on object", e);
		}finally {
			loggerUtil.exitLogger("clickObject");
		}

	}

	public static void selectCheckBox(String locatorType, String value, boolean sCheck) {
		By locator;
		locator = locatorValue(locatorType, value);
		if (driver.findElement(locator).isSelected() && sCheck == false)
			driver.findElement(locator).click();
		else if (!driver.findElement(locator).isSelected() && sCheck == true)
			driver.findElement(locator).click();
	}

	// Get Alert Text
	public static String getAlertText() {
		Alert alt = driver.switchTo().alert();
		String text = alt.getText();
		alt.accept();
		return text;
	}

	// Get text based on Value
	public static String getText(String locatorType, String value) throws Exception {
		String uiText = null;
		try {
			loggerUtil.entryLogger("getText");
			By locator;
			locator = locatorValue(locatorType, value);
			explicitWait(locatorType, value);
			WebElement elementUI = driver.findElement(locator);
			if (elementUI != null && elementUI.isEnabled() && elementUI.isDisplayed())
				uiText = elementUI.getText();
			loggerUtil.LogMessage(LogLevel.INFO, "getText", uiText.toString());
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.SEVERE, "getText", "unable to getTextByID", ex);
		} finally {
			loggerUtil.exitLogger("getText");
		}
		return uiText;
	}

	// Get Attribute Value based on value attribute
	public static String getAttributeTitle(String locatorType, String value) throws Exception {
		String uiText = null;
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			loggerUtil.entryLogger("getAttributeTitle");
			CoreUtil.imSleepy(2000);
			;
			WebElement elementUI = driver.findElement(locator);
			if (elementUI != null && elementUI.isEnabled() && elementUI.isDisplayed())
				uiText = elementUI.getAttribute("title");
			loggerUtil.LogMessage(LogLevel.INFO, "getAttributeClass", uiText.toString());
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.WARN, "getAttributeTitleByXPath", ex.getMessage(), ex);
		} finally {
			loggerUtil.exitLogger("getAttributeTitleByXPath");
		}
		return uiText;
	}

	// Get Attribute Class based on attribute
	public static String getAttributeClass(String locatorType, String value) throws Exception {
		String uiText = null;
		try {
			loggerUtil.entryLogger("getAttributeClass");
			By locator;
			locator = locatorValue(locatorType, value);
			CoreUtil.imSleepy(2000);
			WebElement elementUI = driver.findElement(locator);
			if (elementUI != null && elementUI.isEnabled() && elementUI.isDisplayed())
				uiText = elementUI.getAttribute("class");
			loggerUtil.LogMessage(LogLevel.INFO, "getAttributeClass", uiText.toString());
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.WARN, "getAttributeClass", ex.getMessage(), ex);
		} finally {
			loggerUtil.exitLogger("getAttributeClassByXPath");
		}
		return uiText;
	}

	// Wait for the pop up to load based total number of windows
	public static void waitForPopUpToBeAvailable(int numberOfWindows) {
		try {
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.numberOfWindowsToBe(numberOfWindows));
		} catch (Exception ex) {
			loggerUtil.LogMessage(LogLevel.WARN, "waitForPopUpToBeAvailable", ex.getMessage());
		}
	}

	// Switch to Main Window
	public static void switchToMainWindow() throws Exception {
		try {
			loggerUtil.entryLogger("switchToPopWindow");
			waitForPopUpToBeAvailable(2);
			Set<String> str = driver.getWindowHandles();
			Object[] s = str.toArray();
			driver.switchTo().window(s[0].toString());
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.SEVERE, "switchToPopWindow", "unable to switchToPopWindow", ex);
		} finally {
			loggerUtil.exitLogger("switchToPopWindow");
		}
	}

	// Switch to Main Window
	public static void switchToChildWindow() throws Exception {
		try {
			loggerUtil.entryLogger("switchToPopWindow");
			waitForPopUpToBeAvailable(2);
			Set<String> str = driver.getWindowHandles();
			Object[] s = str.toArray();
			driver.switchTo().window(s[1].toString());
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.SEVERE, "switchToPopWindow", "unable to switchToPopWindow", ex);
		} finally {
			loggerUtil.exitLogger("switchToPopWindow");
		}
	}

	// Switch to window based on specified window object
	public static void switchToWindow(String uiObject) throws Exception {
		try {
			loggerUtil.entryLogger("switchToWindow");
			driver.switchTo().window(uiObject);
			// CoreUtil.imSleepy(5000);
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.SEVERE, "switchToWindow", "unable to switchToWindow", ex);
		} finally {
			loggerUtil.exitLogger("switchToWindow");
		}
	}

	// Switch to default window
	public static void switchToDefaultWindow() throws Exception {
		try {
			loggerUtil.entryLogger("switchToDefaultWindow");
			driver.switchTo().defaultContent();
		} catch (Exception ex) {
			loggerUtil.LogMessage(LogLevel.WARN, "switchToDefaultWindow", "unable to switchToDefaultWindow");
		} finally {
			loggerUtil.exitLogger("switchToDefaultWindow");
		}
	}

	public static void doubleClickOnObject(String locatorType, String value) throws Exception {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			loggerUtil.entryLogger("clickObject");
			CoreUtil.imSleepy(2000);
			WebElement element = driver.findElement(locator);
			Actions a = new Actions(driver);
			a.moveToElement(element).click().doubleClick().build().perform();
		} catch (NoSuchElementException e) {
			loggerUtil.LogSevereMessage(LogLevel.ERROR, "clickObject", "unable to click on object", e);
		} finally {
			loggerUtil.exitLogger("clickObject");
		}
	}

	public static void rightClick(String locatorType, String value) {
		try {
			By locator;
			locator = locatorValue(locatorType, value);
			loggerUtil.entryLogger("clickObject");
			CoreUtil.imSleepy(2000);
			Actions action1 = new Actions(driver);
			action1.moveToElement(element);
			action1.contextClick(element).build().perform(); /* this will perform right click */
			WebElement elementEdit = driver.findElement(locator); /* This will select menu after right click */
			elementEdit.click();
		} catch (NoSuchElementException e) {
			try {
				loggerUtil.LogSevereMessage(LogLevel.ERROR, "clickObject", "unable to click on object", e);
			} catch (Exception e1) {

				e1.printStackTrace();
			}
		} finally {
			loggerUtil.exitLogger("clickObject");
		}
	}

	public static void mouseLeftClick(String locatorType, String element1) {
		try {
			By locator;
			locator = locatorValue(locatorType, element1);
			Actions action = new Actions(driver);
			WebElement element = driver.findElement(locator);
			((JavascriptExecutor) driver).executeScript("arguments[0].scrollIntoView(true);", element);
			Thread.sleep(500);
			action.contextClick(element).perform();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Date Selection
	public static void dateSelect(String datePicker, String date) {
		try {
			driver.findElement(By.xpath(datePicker)).click();
			CoreUtil.imSleepy(2000);
			driver.findElement(By.xpath(date)).click();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// IS element present
	public static boolean isElementPresent(String locatorType, String value) {
		boolean status = false;
		WebElement elementUI = null;
		try {
			loggerUtil.entryLogger("isElementPresent");
			By locator;
			locator = locatorValue(locatorType, value);
			elementUI = driver.findElement(locator);
			if (elementUI != null && elementUI.isEnabled() && elementUI.isDisplayed())
				status = true;
			String str = String.valueOf(status);
			System.out.println(str);
			loggerUtil.entryLogger("Element is avaliable");
		} catch (Exception e) {
			loggerUtil.LogMessage(LogLevel.WARN, "isElementPresent", " element not found");
		} finally {
			loggerUtil.exitLogger("isElementPresent");
		}
		return status;
	}
	
	    // Switch to Frame Window 
		public static void switchToDefaultFrame()  {
			driver.switchTo().defaultContent();
		}

	// Switch to Frame Window by ID
	public static void switchToFrame(String locatorType, String value) throws Exception {
		try {
			loggerUtil.entryLogger("switchToFrameByID");
			driver.switchTo().defaultContent();
			waitForFrameToBeAvailable(locatorType, value);
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.SEVERE, "switchToFrame", "unable to switchToFrame", ex);
		} finally {
			loggerUtil.exitLogger("switchToFrame");
		}
	}

	// Switch to Frame Window by Number
	public static void switchToFrameByNumber(int uiObject) throws Exception {
		try {
			loggerUtil.entryLogger("switchToFrameByNumber");
			driver.switchTo().defaultContent();
			driver.switchTo().frame(uiObject);
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.SEVERE, "switchToFrameByNumber", "unable to switchToFrameByNumber",
					ex);
		} finally {
			loggerUtil.exitLogger("switchToFrameByNumber");
		}
	}

	// Wait for the frame to load based on ID
	public static void waitForFrameToBeAvailable(String locatorType, String value) {
		WebDriverWait wait = new WebDriverWait(driver, 60);
		By locator;
		locator = locatorValue(locatorType, value);
		wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt(locator));
	}

	// explicit wait based on Xpath
	public static void explicitWait(String locatorType, String value) {
		try {
			loggerUtil.entryLogger("explicitWait");
			By locator;
			locator = locatorValue(locatorType, value);
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.visibilityOfElementLocated(locator));
		} catch (Exception ex) {
			loggerUtil.LogMessage(LogLevel.WARN, "explicitWaitByXpath",
					"Explicit wait of 60 seconds over/Exception occured." + ex.getMessage());

		} finally {
			loggerUtil.exitLogger("explicitWaitByXpath");
		}
	}

	public static void hoverElement(String locatorType, String value) throws Exception {
		try {
			loggerUtil.entryLogger("hoverElement");
			By locator;
			locator = locatorValue(locatorType, value);
			explicitWait(locatorType, value);
			Actions hoverAction = new Actions(driver);
			WebElement hoverElement = driver.findElement(locator);
			hoverAction.clickAndHold(hoverElement).perform();
			hoverAction.release(hoverElement); // Perform mouse

			// hover action
			// using
			// 'clickAndHold'
			// method
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.SEVERE, "hoverElementUsingXPath",
					"unable to hover Element using XPath", ex);
		} finally {
			loggerUtil.exitLogger("hoverElementUsingXPath");
		}
	}

	// Close the working browser
	public static void closeBrowser() throws Exception {
		try {
			loggerUtil.entryLogger("closeBrowser");
			driver.close();
			loggerUtil.LogMessage(LogLevel.INFO, "closeBrowser", "browser closed");
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.SEVERE, "closeBrowser", "unable to close browser", ex);
		} finally {
			loggerUtil.exitLogger("closeBrowser");
		}
	}

	public static void quitBrowser() throws Exception {
		try {
			loggerUtil.entryLogger("quitBrowser");
			driver.quit();
		} catch (Exception e) {
			loggerUtil.LogSevereMessage(LogLevel.SEVERE, "quitBrowser", "Unable to quit the browser ", e);
		} finally {
			loggerUtil.exitLogger("quitBrowser");
		}
	}

	// Take screen shot
	public static void getScreenshot() {
		try {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			// The below method will save the screen shot
			FileUtils.copyFile(scrFile, new File("/FrameWork/Screenshots/" + CoreUtil.timeStamp_forFilename() + "/"
					+ String.valueOf(screenshotCounter) + ".jpg"));
			screenshotCounter++;
		} catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	// Take screen shot
	public static void getScreenshot(String methodName) {
		try {
			File scrFile = ((TakesScreenshot) driver).getScreenshotAs(OutputType.FILE);
			// The below method will save the screen shot
			FileUtils.copyFile(scrFile, new File("/FrameWork/Screenshots/" + methodName
					+ CoreUtil.timeStamp_forFilename() + "/" + String.valueOf(screenshotCounter) + ".jpg"));
			screenshotCounter++;
		} catch (Exception ex) {
			ex.printStackTrace();
		}

	}

	// Open the URL/Application
	public static WebDriver navigateURL(String URL) throws Exception {
		try {
			loggerUtil.entryLogger("navigateURL");
			driver.get(URL);
			loggerUtil.LogMessage(LogLevel.INFO, "navigateUrl", "navigated to URL:" + URL);
		} catch (Exception e) {
			loggerUtil.LogSevereMessage(LogLevel.SEVERE, "navigateURL", "unable to navigate URL", e);
		} finally {
			loggerUtil.exitLogger("navigateUrl");
		}
		return driver;

	}

	// Verify the expected text with actual Text based ID
	public static boolean verifyText(String locatorType, String value, String ExpectedText) throws Exception {
		boolean Status = false;
		String Text = null;
		try {
			loggerUtil.entryLogger("verifyText");
			By locator;
			locator = locatorValue(locatorType, value);
			explicitWait(locatorType, value);
			WebElement elementUI = driver.findElement(locator);
			Text = elementUI.getText();
			Status = false;
			if (ExpectedText.equals(Text))
				Status = true;
			String str = String.valueOf(Status);
			System.out.println(str);
			loggerUtil.entryLogger("Element is avaliable");
			loggerUtil.LogMessage(LogLevel.DEBUG, "verifyText", "Text from element: " + Text);
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.ERROR, "verifyText", "unable to verift text", ex);
		} finally {
			loggerUtil.exitLogger("verifyText");
		}
		return Status;
	}

	// Clear the Textbox field and Passing some data to Textbox
	public static String ClearAndSetText(String locatorType, String value, String text) {
		try {
			loggerUtil.entryLogger("ClearAndSetText");
			By locator;
			locator = locatorValue(locatorType, value);
			explicitWait(locatorType, value);
			WebElement element = driver.findElement(locator);
			Actions navigator = new Actions(driver);
			navigator.click(element).sendKeys(Keys.END).keyDown(Keys.SHIFT).sendKeys(Keys.HOME).keyUp(Keys.SHIFT)
					.sendKeys(Keys.BACK_SPACE).sendKeys(text).perform();
			// pNode.info("Passing data to textbox :" + text);
			loggerUtil.LogMessage(LogLevel.INFO, "ClearAndSetText", "To Element: " + text);
		} catch (Exception e) {
			try {
				loggerUtil.LogSevereMessage(LogLevel.ERROR, "Clear And Set Text", "unable to Clear and set text", e);
			} catch (Exception e1) {
				e1.printStackTrace();
			}
		} finally {
			loggerUtil.exitLogger("ClearAndSetText");
		}
		return text;
	}

	// Explicit wait for clickable object by XPath
	public static void explicitWaitByXpathClickable(String uiObject) {
		try {
			loggerUtil.entryLogger("explicitWaitByXpathClickable");
			WebDriverWait wait = new WebDriverWait(driver, 60);
			wait.until(ExpectedConditions.elementToBeClickable(By.xpath(uiObject)));
		} catch (Exception ex) {
			loggerUtil.LogMessage(LogLevel.WARN, "explicitWaitByXpathClickable",
					"Explicit wait of 60 seconds over/Exception occured." + ex.getMessage());
		} finally {
			loggerUtil.exitLogger("explicitWaitByXpathClickable");
		}
	}

	// method to drag and drop elements
	public static void dragAndDrop(String sourceElement, String destinationElement) throws Exception {
		try {
			loggerUtil.entryLogger("dragAndDrop");
			explicitWaitByXpathClickable(sourceElement);
			explicitWaitByXpathClickable(destinationElement);
			WebElement sourceWE = driver.findElement(By.xpath(sourceElement));
			WebElement destWE = driver.findElement(By.xpath(destinationElement));
			if (sourceWE.isDisplayed() && destWE.isDisplayed()) {
				Actions action = new Actions(driver);
				action.dragAndDrop(sourceWE, destWE).build().perform();
			} else {
				System.out.println("Element was not displayed to drag");
			}
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.FATAL, "dragAndDrop", ex.getMessage(), ex);
		} finally {
			loggerUtil.exitLogger("dragAndDrop");
		}
	}

	public static void powerSignoff(String locator1, String locator2) {
		try {
			Actions navigator = new Actions(driver);
			WebElement e1 = driver.findElement(By.xpath(locator1));
			WebElement e2 = driver.findElement(By.xpath(locator2));
			navigator.click(e1).keyDown(Keys.CONTROL).click(e2).build().perform();

		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	// Clear the Textbox field and Passing some data to Textbox and Click on Enter
	public static void ClearAndSetTextEnter(String locatorType, String value, String text) {
		By locator;
		locator = locatorValue(locatorType, value);
		WebElement element = driver.findElement(locator);
		Actions navigator = new Actions(driver);
		navigator.click(element).sendKeys(Keys.END).keyDown(Keys.SHIFT).sendKeys(Keys.HOME).keyUp(Keys.SHIFT)
				.sendKeys(Keys.BACK_SPACE).sendKeys(text).sendKeys(Keys.ENTER).perform();
		CoreUtil.imSleepy(5000);

	}

	public static String getRandomNumbers() {
		char[] chars = { '5', '6', '7', '8', '9', '0' };
		StringBuilder sb = new StringBuilder();
		Random random = new Random();
		for (int i = 0; i < 6; i++) {
			char c = chars[random.nextInt(chars.length)];
			sb.append(c);
		}
		String output = sb.toString();
		return output;
	}

	public static String getRandomString() {
		String chars = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
		StringBuilder builder = new StringBuilder();
		Random rnd = new Random();
		while (builder.length() < 10) {
			int index = (int) (rnd.nextFloat() * chars.length());
			builder.append(chars.charAt(index));
		}
		String str = builder.toString();
		return str;
	}

	// select Text from drop down based on visible text
	public static void selectByText(String locatorType, String value, String Option) throws Exception {
		try {
			loggerUtil.entryLogger("selectByTextUsingID");
			By locator;
			locator = locatorValue(locatorType, value);
			explicitWait(locatorType, value);
			WebElement usrs = driver.findElement(locator);
			Select usr = new Select(usrs);
			Thread.sleep(1000);
			usr.selectByVisibleText(Option);
			loggerUtil.LogMessage(LogLevel.INFO, "selectByTextUsingID", "Option " + Option + " selected");
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.ERROR, "selectByTextUsingID", "unable to select by text", ex);
		} finally {
			loggerUtil.exitLogger("selectByTextUsingID");
		}
	}

	// Navigate to Previous Page
	public static void navigateTopreviousPage() {
		driver.navigate().back();
	}

	// select Text from drop down based on visible text
	public static void selectByTextByIndex(String locatorType, String value, int Option) throws Exception {
		try {
			loggerUtil.entryLogger("selectByTextUsingID");
			By locator;
			locator = locatorValue(locatorType, value);
			explicitWait(locatorType, value);
			WebElement usrs = driver.findElement(locator);
			Select usr = new Select(usrs);
			Thread.sleep(1000);
			usr.selectByIndex(Option);
			loggerUtil.LogMessage(LogLevel.INFO, "selectByTextUsingID", "Option " + Option + " selected");
		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.ERROR, "selectByTextUsingID", "unable to select by text", ex);
		} finally {
			loggerUtil.exitLogger("selectByTextUsingID");
		}

	}

	public static void fetchingTableDataUsingTRAndTDTags(String locatorType, String value) throws Exception {
		try {
			// int count=0;
			loggerUtil.entryLogger("fetchingTableDataUsingTRAndTDTags");
			By locator;
			locator = locatorValue(locatorType, value);
			explicitWait(locatorType, value);
			driver.findElement(locator);
			List<WebElement> rows = driver.findElements(By.tagName("tr"));
			for (int i = 0; i <= rows.size() - 1; i++) {
				List<WebElement> columns = rows.get(i).findElements(By.tagName("td"));
				for (int j = 0; j < columns.size() - 1; j++) {
					// System.out.print(columns.get(j).getText()+" || ");
					System.out.print(columns.get(j).getText() + " -- ");
					// count++;
				}
				System.out.println();
			}
			// System.out.println(count);

		} catch (Exception ex) {
			loggerUtil.LogSevereMessage(LogLevel.ERROR, "fetchingTableDataUsingTRAndTDTags",
					"unable to locate on object", ex);
		} finally {
			loggerUtil.exitLogger("fetchingTableDataUsingTRAndTDTags");
		}
	}

}
